/**
 * This pipeline will build and deploy a Docker image with Kaniko
 * https://github.com/GoogleContainerTools/kaniko
 * without needing a Docker host
 *
 * You need to create a jenkins-docker-cfg secret with your docker config
 * as described in
 * https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/#create-a-secret-in-the-cluster-that-holds-your-authorization-token
 */
// #debug-539ddefcae3fd6b411a95982a830d987f4214251
podTemplate(yaml: """
kind: Pod
spec:
  containers:
  - name: kaniko
    image: gcr.io/kaniko-project/executor:debug-v0.15.0
    imagePullPolicy: Always
    command:
    - /busybox/cat
    tty: true
    volumeMounts:
      - name: jenkins-docker-cfg
        mountPath: /kaniko/.docker
  volumes:
  - name: jenkins-docker-cfg
    projected:
      sources:
      - secret:
          name: jenkins-docker-cfg
          items:
            - key: .dockerconfigjson
              path: config.json
"""
  ) {

  node(POD_LABEL) {
    def commit = checkout(scm).GIT_COMMIT
    stage('Build The Baseimage') {
      container('kaniko') {
        //sh "/kaniko/executor -f `pwd`/Dockerfile.base -c `pwd` --insecure --skip-tls-verify --cache=true --destination=p3s-harbor-harbor-core.baseline.svc/library/baseline --build-arg VCS_REF=${commit}"
        ansiColor('xterm') {
        sh """
            /kaniko/executor \
                -f `pwd`/Dockerfile.base \
                -c `pwd` \
                --insecure \
                --skip-tls-verify \
                --insecure-registry \
                --insecure-pull \
                --cache=true \
                --digest-file=/dev/termination-log \
                --destination=harbor.172-28-128-4.sslip.io/library/baseline \
                --build-arg VCS_REF=${commit}
        """
        sh "cat /dev/termination-log"
        }
      }
    }

        stage('Build with ansible') {
          container('kaniko') {
            sh """
                /kaniko/executor \
                    -f `pwd`/Dockerfile.buildhelper \
                    -c `pwd` \
                    --skip-tls-verify \
                    --skip-tls-verify-pull \
                    --skip-tls-verify-registry \
                    --cache=true \
                    --digest-file=/dev/termination-log \
                    --destination=harbor.172-28-128-4.sslip.io/library/buildhelper \
                    --build-arg VCS_REF=${commit}
            """
            sh "cat /dev/termination-log"
          }
        } 
        stage('Build with python') {
          container('kaniko') {
            sh """
                /kaniko/executor \
                    -f `pwd`/Dockerfile.python \
                    -c `pwd` \
                    --skip-tls-verify \
                    --skip-tls-verify-pull \
                    --skip-tls-verify-registry \
                    --cache=true \
                    --digest-file=/dev/termination-log \
                    --destination=harbor.172-28-128-4.sslip.io/library/build-python \
                    --build-arg VCS_REF=${commit}
            """
            sh "cat /dev/termination-log"
          }
        }
      }
    
  
}